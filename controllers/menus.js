const model = require("../models");
const menusModel = model.menu;

exports.createMenu = (req, res) => {
	const { id_partner, name, image, stock, price, is_active } = req.body;
	menusModel
		.findOne({
			where: {
				id_partner,
				name,
			},
		})
		.then((menusAlready) => {
			if (menusAlready) {
				res.send({
					error: true,
					message: "Menu already exist",
					data: {
						name: menusAlready.name,
						price: menusAlready.price,
						stock: menusAlready.stock,
					},
				});
			} else {
				const dataInput = {
					id_partner,
					name,
					image,
					stock,
					price,
					is_active,
				};
				console.log(dataInput);
				menusModel
					.create(dataInput)
					.then((responseData) => {
						res.send({
							error: false,
							message: "Menu already publish",
							data: responseData,
						});
					})
					.catch((e) => {
						console.log(e);
						res.status(500).json({
							message: "Internal Server Error1 Create",
							error: true,
						});
					});
			}
		})
		.catch((e) => {
			console.log(e);
			res.status(500).json({
				message: "Internal Server Error Models",
				error: true,
			});
		});
};

exports.getAllMenu = (req, res) => {
	const { id } = req.params;

	menusModel
		.findAll({
			where: {
				id_partner: id,
			},
		})
		.then((response) => {
			if (response)
				return res.send({
					error: false,
					message: "Menu by id",
					data: response,
				});

			res.send({
				message: "Not find Data",
				error: true,
			});
		})
		.catch((e) => {
			console.log(e);
			res.status(500).json({
				message: "Internal Server Error Models",
				error: true,
			});
		});
};
