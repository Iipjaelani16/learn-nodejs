const dotEnv = require("dotenv");
const { v4: uuidv4 } = require("uuid");
const jwt = require("jsonwebtoken");

const model = require("../models");
const UsersModel = model.user;
const myEnv = dotEnv.config();
const jwtKey = "my_secret_key";
const jwtExpirySeconds = 300;

const createJwt = (username) => {
	const token = jwt.sign({ username }, jwtKey, {
		algorithm: "HS256",
		expiresIn: jwtExpirySeconds,
	});
	console.log(token, ":TOKEN");
	return token;
};

exports.Register = (req, res) => {
	const {
		email, //
		password, //
		username, //
		first_name, //
		last_name, //
		gender, //
		image, //
		token, //
	} = req.body;
	UsersModel.findOne({
		where: {
			email,
		},
	}).then((alreadyData) => {
		if (alreadyData) {
			return res.send({
				message: "User Already Exist",
				error: true,
			});
		}
		const UUID = uuidv4();
		const createToken = createJwt(username);
		const dataInput = {
			unix_id: UUID, //
			email: email, //
			password: password, //
			username: username, //
			first_name: first_name, //
			last_name: last_name, //
			gender: gender, //
			image: image, //
			token: createToken, //
			role_id: 3, //
			is_active: 1, //
		};
		UsersModel.create(dataInput).then((response) => {
			res.send({
				message: "Register OK",
				data: response,
			});
		});
	});
};

exports.Login = (req, res) => {
	const { username, password } = req.body;
	if (!username) {
		return res.status(401).end();
	}

	const token = jwt.sign({ username }, jwtKey, {
		algorithm: "HS256",
		expiresIn: jwtExpirySeconds,
	});
	console.log("token:", token);

	res.cookie("token", token, { maxAge: jwtExpirySeconds * 1000 });
	res.end();
};

const handleSplit = (token) => {
	var parts = token.split("=");
	return parts[1];
};

exports.Welcome = (req, res) => {
	const data = req.headers.cookie;
	var token;
	if (data) {
		token = handleSplit(data);
	}
	if (!token) {
		res.send({
			error: true,
			message: "you don't authorized",
		});
		return res.status(401).end();
	}
	var payload;
	try {
		payload = jwt.verify(token, jwtKey);
	} catch (e) {
		if (e instanceof jwt.JsonWebTokenError) {
			return res.status(401).end();
		}
		return res.status(400).end();
	}
	res.send(`Welcome ${payload.username}!`);
};

exports.Refresh = (req, res) => {
	const data = req.headers.cookie;
	var token;
	if (data) {
		token = handleSplit(data);
	}
	if (!token) {
		return res.status(401).end();
	}

	var payload;
	try {
		payload = jwt.verify(token, jwtKey);
	} catch (e) {
		if (e instanceof jwt.JsonWebTokenError) {
			return res.status(401).json();
		}
		return res.status(400).end();
	}
	const nowUnixSeconds = Math.round(Number(new Date()) / 1000);
	if (payload.exp - nowUnixSeconds > 30) {
		return res.status(400).end();
	}

	const newToken = jwt.sign({ username: payload.username }, jwtKey, {
		algorithm: "HS256",
		expiresIn: jwtExpirySeconds,
	});

	res.cookie("token", newToken, {
		maxAge: jwtExpirySeconds * 1000,
	});
	res.end();
};
