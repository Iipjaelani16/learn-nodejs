# LEARN NODE JS

---

### BOILER PLATE

- [x] NodeJS
- [x] ExpressJS
- [x] Sequelize
- [x] MySQL

## Getting Started

```bash
git clone https://gitlab.com/Iipjaelani16/learn-nodejs
npm install --save
npx sequelize db:migrate
npm start
```

## Create DB and Setting config.json

```json
	"development": {
		"username": "username DB",
		"password": "password DB",
		"database": "name DB",
		"host": "127.0.0.1",
		"port": 8080,
		"dialect": "mysql",
		"operatorsAliases": true
	}
```

## Example POST data

```json

POST /api/v1/menu HTTP/1.1
Host: localhost:8588
Content-Type: application/json

{
    "id_partner": "0010101",
    "name":"Tahu",
    "image": "http://localhost:8080/assets/images/tahu.jpg",
    "stock": "unlimited",
    "price":12000 ,
    "is_active": 1
}

```

## Example create table

```bash
npx sequelize model:generate --name category --attributes name_category:string,is_active:integer

```

**License** [APACHE LICENSE, VERSION 2.0](https://www.apache.org/licenses/LICENSE-2.0)
