require("express-group-routes");

//initial express
const express = require("express");
//make variable
const App = express();
//add port
const port = 8588;

//modules
const bodyParser = require("body-parser");
//json initial
App.use(bodyParser.json());

//create home url
App.get("/", (req, res) => {
	// what is HOME ?
	res.send("HELLO EXPRESS");
});

//controller
const MenusController = require("./controllers/menus");
const UsersController = require("./controllers/users");

//make group routers
App.group("/api/v1", (routes) => {
	routes.post("/register", UsersController.Register);
	routes.post("/login", UsersController.Login);
	routes.get("/welcome", UsersController.Welcome);
	routes.post("/refresh", UsersController.Welcome);
	routes.post("/menu", MenusController.createMenu);
	routes.get("/menus/:id", MenusController.getAllMenu);
});

App.get("/people", (req, res) => {
	res.send(dummy);
});

App.listen(port, () => console.log(`running in port ${port}!!!!!`));
